package ru.tsc.kyurinova.tm.api.service;

import ru.tsc.kyurinova.tm.model.Project;
import ru.tsc.kyurinova.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    Task bindTaskById(String userId, String projectId, String taskId);

    Task unbindTaskById(String userId, String projectId, String taskId);

    void removeAllTaskByProjectId(String userId, String projectId);

    Project removeById(String userId, String projectId);

    List<Task> findAllTaskByProjectId(String userId, String projectId);

}
