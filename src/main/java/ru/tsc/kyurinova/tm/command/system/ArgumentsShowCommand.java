package ru.tsc.kyurinova.tm.command.system;

import ru.tsc.kyurinova.tm.command.AbstractCommand;
import ru.tsc.kyurinova.tm.model.Command;

import java.util.Collection;

public class ArgumentsShowCommand extends AbstractCommand {

    @Override
    public String name() {
        return "arguments";
    }

    @Override
    public String arg() {
        return "-arg";
    }

    @Override
    public String description() {
        return "Display list of arguments...";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        for (final AbstractCommand command : serviceLocator.getCommandService().getCommands()) {
            String argument = command.arg();
            if (argument != null && !argument.isEmpty())
                System.out.println(argument + ": " + command.description());
        }
    }
}
