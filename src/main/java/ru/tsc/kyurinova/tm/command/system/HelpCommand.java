package ru.tsc.kyurinova.tm.command.system;

import ru.tsc.kyurinova.tm.command.AbstractCommand;
import ru.tsc.kyurinova.tm.model.Command;

import java.util.Collection;

public class HelpCommand extends AbstractCommand {

    @Override
    public String name() {
        return "help";
    }

    @Override
    public String arg() {
        return "-h";
    }

    @Override
    public String description() {
        return "Display all commands with description...";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        for (final AbstractCommand command : serviceLocator.getCommandService().getCommands())
            System.out.println(command.name() + ": " + command.description());
    }
}
